import asyncio
import time
from loguru import logger
from aiohttp import ClientSession
from playwright.async_api import async_playwright
import sys

logger.remove(0)
logger.add(
    sys.stderr,
    format="| <green>{time:HH:mm:ss.SS}</green> "
           "| {level} "
           "| <green>{extra[method]}</green> "
           "| {extra[url]} "
           "| {extra[post_data]} ",
    colorize=True
)


async def tracking_request(request):
    context_logger = logger.bind(
        method = request.method,
        url = request.url,
        post_data = request.post_data
    )
    context_logger.info("request info")