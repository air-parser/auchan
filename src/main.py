import math
import time
import asyncio
import orjson

from playwright.async_api import async_playwright
from parsel import Selector
from aiohttp import ClientSession
from logger import tracking_request


async def get_cookies(url: str):

    async with async_playwright() as playwright:
        browser = await playwright.firefox.launch()
        context = await browser.new_context()
        page = await context.new_page()
        page.on("request", tracking_request)

        await page.goto(url)

        # TODO найти где подставляются куки
        async with page.expect_request(lambda request: "https://www.auchan.ru/scripts/runtime.2957.js" in request.url, timeout = 5 * 1000):
            pass

        cookies = {
            cookie['name']: cookie['value']
            for cookie in await context.cookies()
        }
        return cookies


async def get_categories(session: ClientSession):
    response = await session.get('https://www.auchan.ru/catalog/')
    selector = Selector(text = await response.text())

    data = selector.xpath('//script[@id="init"]/text()').get()
    data = data.replace("window.__INITIAL_STATE__ = ", '')
    data = orjson.loads(data)

    for category in data.get('categories', {}).get('categories', {}):
        yield category.get('name'), category.get('code'), category.get('productsCount')


async def get_products(num_page: int, code: str, session: ClientSession):
    params = {
        'merchantId': '1',
        'page': num_page,
        'perPage': '100',
    }
    data = {'filter': {
        'category': code,
        'promo_only': False,
        'active_only': False,
        'cashback_only': False
    }}
    response = await session.post('https://www.auchan.ru/v1/catalog/products', params=params, json=data)
    data = await response.json()

    items = data.get('items')
    for item in items:
        yield item


async def start():

    cookies = await get_cookies('https://www.auchan.ru/catalog/')

    headers = {
        'Host': 'www.auchan.ru',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/110.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
        'Upgrade-Insecure-Requests': '1',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'cross-site',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
    }

    async with ClientSession(cookies=cookies, headers=headers) as session:

        async for name, code, total in get_categories(session):

            for num_page in range(1, math.ceil(total / 100) + 1):
                async for item in get_products(num_page, code, session):
                    print(item)


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
loop.run_until_complete(start())
